// 频率间隔执行
function ExcuInterval () {
  let status = 0
  return function (fn, time) {
    if (status) return
    status = 1
    setTimeout(function () {
      fn()
      status = 0
    }, time)
  }
}
let excu = ExcuInterval()

// 拿到html
var style = document.documentElement.style
// 执行函数
function run () {
  // 视口宽度
  var w = innerWidth
  if (!w) return
  // 苹果5设计稿
  // docEl.style.fontSize=20/320*clientWidth+"px";
  // 苹果6设计稿   375/23
  if (w > 640) w = 640
  style.fontSize = 23 / 375 * w + 'px'
}
// 触发
addEventListener('resize', () => { excu(run, 1000) })
run()
