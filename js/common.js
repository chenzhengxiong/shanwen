/*
* nifty.js
* Created by lishu
* 2018-01-08
* */


($.extend({
  // jQuery.msg
  /*
   * 参数一：消息的内容，String, 默认为空, 必填
   * 参数二：消息类型，String, : info, success，error, warn 默认为info， 选填
   * 参数三：消息停留时间, Number, 默认为2000， 选填
   */
  msg: function(str, type, timeout) {
    if ($(".msg").length > 0) {
      return false;
    } else {
      if (str.trim() === '') {
        return false;
      } else {
        $("body").append(`<div class='msg ${type ? type : "info"}'>${str}</div>`);
        $(".msg").stop().animate({top:"0px",opacity:"1"}, "fast");
        setTimeout(function () {
          $(".msg").animate({top:"-1rem",opacity:".3"}, "fast", "linear", function () {
            $(".msg").remove();
          });
        }, timeout ? timeout : 2000);
      }
    }
  },
  // jQuery.toast
  /*
   * 参数一：消息的内容，String, 默认为空, 必填
   * 参数二：消息停留时间, Number, 默认为2000， 选填
   */
  toast: function(str, timeout) {
    if ($(".toast").length > 0) {
      return false;
    } else {
      if (str.trim() === '') {
        return false;
      } else {
        let delay = timeout || 2000;
        $("body").append(`<div class='toast'>${str}</div>`);
        $(".toast").animate({opacity:"1"}, "fast").delay(delay).animate({opacity:"0"}, "fast", "linear", function () {
          $(this).remove();
        });

      }
    }
  },
  /* jQuery.confirm */
  /*
  * 参数一：提示框的标题，String, 默认为空，必填
  * 参数二：提示框的文本内容，String,默认为空，必填
  * 参数三：点击确定的回调函数，function,选填
  */
  confirm: function (title, content, callback) {
    if ($(".confirm").length > 0) {
      return false;
    } else {
      let html = `<div class="confirm">
                  <div class="confirm-mask"></div>
                  <div class="confirm-main">
                    <div class="confirm-top">${title}</div>
                    <div class="confirm-body">${content}</div>
                    <div class="confirm-bottom">
                      <div class="confirm-btn confirm-confirm J_confirm_confirm">确定</div>
                      <div class="confirm-btn confirm-cancel J_confirm_cancel">取消</div>
                    </div>
                  </div>
                </div>`;
      $("body").append(html);
      $(".confirm").show("fadeIn");
      $(document).on("click", ".confirm-btn", function () {
        $(".confirm").hide("fadeOut");
        setTimeout(function () {
          $(".confirm").remove();
        }, 1000);
      });
      $(document).on("click", ".J_confirm_confirm", function () {
        typeof(callback) === "function" && callback();
      });
    }
  }
}))(jQuery);


/* fastclick */
$(function() {
  FastClick.attach(document.body);
});